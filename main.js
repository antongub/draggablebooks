const covers = [
    'https://about.canva.com/wp-content/uploads/sites/3/2015/01/art_bookcover.png',
    'https://marketplace.canva.com/MADOPgSRvyE/3/0/thumbnail_large/canva-rust-orange-lioness-vintage-book-cover-MADOPgSRvyE.jpg',
    'https://emusdebuts.files.wordpress.com/2018/04/the-benefits-of-being-an-octopus.jpg?w=341&h=512',
    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSfG15FmFiRQC4dg9Guinkmrws2iK_2WwBF7rBZUbt10MIwIlEH&s',
];

// https://stackoverflow.com/a/12646864
function shuffleArray(array) {
    for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];
    }
}

const background = document.getElementById('background');

class Book {
    constructor(imageUrl, index) {
        this.id = index;
        const rotateX = Math.random() < 0.5 ? -1 : 1;
        this.rotation = (Math.random() * 100) * rotateX;

        this.element = document.createElement('div');
        this.element.className = 'book';
        this.element.style.transform = `rotate(${this.rotation}deg)`;
        this.imgElement = document.createElement('img');
        this.imgElement.className = 'bookcover';
        this.imgElement.src = imageUrl;
        this.element.appendChild(this.imgElement);
    }

    render() {
        background.appendChild(this.element);
    }
}

async function start() {
    let mouseDown = false;
    let targetBook;
    let allBooks = [];

    await shuffleArray(covers);
    await covers.forEach((element, index) => {
        const bookcover = new Book(element, index);
        bookcover.element.style.zIndex = `${bookcover.id}`;
        bookcover.element.onmousedown = () => {
            mouseDown = true;
            bookcover.element.style.transform = `rotate(${bookcover.rotation}deg) scale(1.1)`;
            bookcover.element.style.zIndex = `${covers.length}`;
            targetBook = bookcover;
        };
        allBooks.push(bookcover);
        bookcover.render();
    });

    window.addEventListener('mouseup', () => {
        mouseDown = false;
        targetBook.element.style.transform = `rotate(${targetBook.rotation}deg)`;

        // placing the newest book on the top and placing the rest right under it
        for (let i = covers.length - 1; i > targetBook.id; i--) {
            let cur = allBooks.find(e => e.id === i);
            cur.newid = i - 1;
        }
        allBooks.forEach(element => {
            if (element.newid >= 0) {
                element.id = element.newid
            }
            element.element.style.zIndex = `${element.id}`;
        });
        targetBook.id = covers.length - 1;
        targetBook.element.style.zIndex = `${covers.length - 1}`;
    })
    window.addEventListener('mousemove', function () {
        if (!mouseDown) return;
        targetBook.element.style.top = (event.y - (targetBook.element.offsetHeight / 2)) + "px";
        targetBook.element.style.left = (event.x - (targetBook.element.offsetWidth / 2)) + "px";
    })
}

start();
